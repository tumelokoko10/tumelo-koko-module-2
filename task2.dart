void main(List<String> args) {
  List<String> Winners = [
    '2021 - Ambani Afrika',
    '2020 - Easy Equities',
    '2019 - Naked Insurance',
    '2018 - Khula!',
    '2017 - Shyft',
    '2016 - Domestly',
    '2015 - Wumdrop',
    '2014 - Live Inspect',
    '2013 - SnapScan',
    '2012 - FNB Banking App'
  ];

  Winners.sort();
  String app;
  String name;
  int year;

  for (int index = 0; index < Winners.length; index++) {
    app = Winners[index];
    year = getYear(app);
    name = getName(app);
    if(year==2017 || year==2018){
      print("The overall winning app of $year is $name.");
    }    
  }

  int N = Winners.length;
  print("There have been $N winners of the MTN App Academy Awards since 2012.");
}

String getName(String app) {
  return app.substring(7);
}

int getYear(String app) {
  return int.parse(app.substring(0, 4));
}
