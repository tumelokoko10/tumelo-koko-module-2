class App {
  late String name;
  late String category;
  late String developers;
  late int year;

  void printDetails() {
    print("Winner: $name");
    print("Developed by $developers");
    print("Year: $year");
    print("Category: $category");
  }

  String allCaps() {
    var all_caps = name.toUpperCase();
    print(all_caps);
    return all_caps;
  }
}

void main(List<String> args) {
  App dom = App();
  dom.name = "Domestly";
  dom.year = 2016;
  dom.category = "Consumer App";
  dom.developers = "Thatoyoana Marumo and Berno Potgieter";
  dom.printDetails();
  dom.allCaps();
}
