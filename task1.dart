import 'dart:io';

void main(List<String> args) {
  print("What is your name?");
  var name = stdin.readLineSync();

  print("What is your favourite app?");
  var app = stdin.readLineSync();

  print("Which city are you from?");
  var city = stdin.readLineSync();

  print(
      "Hey there $name! Nice to know you, how's the weather in $city?\nFor real? $app is my favourite app too!");
}
